export class Cell {
  private currentValue: number;
  constructor(value: number){
    this.currentValue = value;
  }
  setDisplayValue(value: number): void {
    this.currentValue = value;
  }
  changeDisplayValue(): void {
    if (this.currentValue === 0) {
      this.currentValue = 1;
    }
    else {
      this.currentValue = 0;
    }
  }
  getDisplayValue(): number {
    return this.currentValue;
  }
}
