export class Board {
  private boardHeight: number;
  private boardWidth: number;
  private board: number[][];
  private generation: number;
  constructor(height: number, width: number) {
    this.boardHeight = height;
    this.boardWidth = width;
    this.board = [];
    this.generation = 0;
    this.initializeBoard();
  }
  initializeBoard(): void {
    this.generation = 0;
    // Creacion de array 2D: 1 dimension posee listas y dentro de cada lista hay celulaas con valores 0 al principio
    for (let x = 0; x < this.boardWidth; x++) {
      this.board[x] = [];
      for (let y = 0; y < this.boardHeight; y++) {
        this.board[x][y] = 0;
      }
    }
  }

  status(coordinateX: number, coordinateY: number): number {// Devuelve el valor actual de la celula
    return this.board[coordinateX][coordinateY];
  }
  changeCellValue(coordinateX: number, coordinateY: number): void {// Si el valor es 0 cambia a 1 y si es 1 cambia a 0
    this.board[coordinateX][coordinateY] = this.board[coordinateX][coordinateY] === 0 ? 1 : 0;
  }
  getBoardArray(): number[][] {// Devuelve el array 2D
    return this.board;
  }
  nextGen(): number {
    this.generation++; // Aumento la generacion actual
    const tempBoard = [] as any; // Creo array temporal
    for (let x = 0; x < this.board.length; x++) { // Para la dimension x crear una lista vacia en el array temporal
      tempBoard[x] = [];
      for (let y = 0; y < this.board[x].length; y++) { // Para la dimension x,y asignar el valor 0 o 1 dependiendo de checkRules
        tempBoard[x][y] = this.checkRules(x, y);
      }
    }
    this.board = [...tempBoard]; // Asignar el array temporal a this.board
    return this.generation;
  }
  getGeneration(): number {
    return this.generation;
  }
  checkRules(coordinateX: number, coordinateY: number): number {
    const width = this.board.length;
    const height = this.board[0].length;
    // Comienzo a hacer el checkeo de celdas vecinas, si se termina el tablero, tomo la celda del otro lado
    // Son 8 porque se tienen en cuenta los vecinos diagonales
    const xLeft = coordinateX - 1 < 0 ? width - 1 : coordinateX - 1;
    const xRight = coordinateX + 1 >= width ? 0 : coordinateX + 1;
    const yUp = coordinateY - 1 < 0 ? height - 1 : coordinateY - 1;
    const yDown = coordinateY + 1 >= height ? 0 : coordinateY + 1;
    const currentValue = this.board[coordinateX][coordinateY];
    const neighbors =
      this.board[xLeft][yDown] +
      this.board[xLeft][coordinateY] +
      this.board[xLeft][yUp] +
      this.board[coordinateX][yDown] +
      this.board[coordinateX][yUp] +
      this.board[xRight][yDown] +
      this.board[xRight][coordinateY] +
      this.board[xRight][yUp];
    // Basandome en las reglas sacadas de Wikipedia se realizan los siguientes checkeos
    /*Any live cell with two or three live neighbours survives.*/
    if (currentValue === 1 && (neighbors === 2 || neighbors === 3)) {
      return 1;
    }
      // Any dead cell with three live neighbours becomes a live cell.
    if (currentValue === 0 && neighbors === 3) {
      return 1;
    }
    /*All other live cells die in the next generation. Similarly, all other dead cells stay dead.*/
    else {
      return 0;
    }
  }
}
