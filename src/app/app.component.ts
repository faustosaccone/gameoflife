import {Component, OnInit} from '@angular/core';
import {Board} from './models/board';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'conway';
  board: Board;
  playing: number;
  rows: number;
  cols: number;
  intervalTime: number;
  timer: any;
  constructor() {
    this.cols = 5; // Columnas del array
    this.rows = 5; // Filas del array
    this.board = new Board(this.cols, this.rows); // Genera un tablero
    this.playing = -1; // Flag de control de estado de juego
    this.intervalTime = 300; // Intervalo entre generaciones
  }
  ngOnInit(): void {
    this.generateInterval();
  }
  changeCellValue(coordinateX: number, coordinateY: number): void{// Al clickear sobre una celula se cambia el valor, valores posibles 0 o 1
    this.board.changeCellValue(coordinateX, coordinateY);
  }
  startGame(): void { // Comenzar el juego
    this.playing = 0;
  }
  pauseResumeGame(): void {// Pausar juego/ seguir jugando donde estaba
    this.playing = this.playing === 0 ? 1 : 0;
  }
  restartGame(): void {// Reiniciar el juego, tablero con todos 0
    this.playing = -1;
    this.board.initializeBoard();
  }
  generateInterval(): void{// Funcion para generacion de intervalos de tiempo
    const self = this;
    this.timer = setInterval(function repeater(): void {// Funcion recursiva que checkea el tiempo entre intervalos y detecta cambios en DOM
      if (self.playing === 0) {
        self.board.nextGen();
        clearInterval(self.timer);
        self.timer = setInterval(repeater, self.intervalTime);
      }
    }, this.intervalTime);
  }
  setIntervalTime(intervalInput: string): void {
    // Si el string ingresado es distinto a '' entonces parsea el numero ingresado y pasa a ser el nuevo intervalo de tiempo
    if (intervalInput !== '') {
      this.intervalTime = parseInt(intervalInput, 10) <= 0 ? this.intervalTime : parseInt(intervalInput, 10);
    }
  }
  changeBoard(heightInput: string, widthInput: string): void {
    // Toma el input desde DOM y luego crea un nuevo tablero con esas dimensiones
    if (heightInput !== '' && widthInput !== '') {
      this.rows = parseInt(heightInput, 10) <= 5 ? this.rows : parseInt(heightInput, 10);
      this.cols = parseInt(widthInput, 10) <= 5 ? this.cols : parseInt(widthInput, 10);
      this.restartGame();
      this.board = new Board(this.cols, this.rows);
    }
  }
  // Probar localStorage
  set(key: string, data: any): void {
    try {
      localStorage.setItem(key, JSON.stringify(data));
    }
    catch (e) {
      console.log(e);
    }
  }
  get(key: string): any {
    try {
      localStorage.getItem(key);
    }
    catch (e) {
      console.log(e);
    }
  }
}
